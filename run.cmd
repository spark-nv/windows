@echo off

setlocal enabledelayedexpansion

REM Get the directory of the currently running script
set "script_dir=%~dp0"

:: BatchGotAdmin
:-------------------------------------
REM  --> Check for permissions
    IF "%PROCESSOR_ARCHITECTURE%" EQU "amd64" (
>nul 2>&1 "%SYSTEMROOT%\SysWOW64\cacls.exe" "%SYSTEMROOT%\SysWOW64\config\system"
) ELSE (
>nul 2>&1 "%SYSTEMROOT%\system32\cacls.exe" "%SYSTEMROOT%\system32\config\system"
)

REM --> If error flag set, we do not have admin.
if '%errorlevel%' NEQ '0' (
    echo Requesting administrative privileges...
    goto UACPrompt
) else ( goto gotAdmin )

:UACPrompt
    echo Set UAC = CreateObject^("Shell.Application"^) > "%temp%\getadmin.vbs"
    set params= %*
    echo UAC.ShellExecute "cmd.exe", "/c ""%~s0"" %params:"=""%", "", "runas", 1 >> "%temp%\getadmin.vbs"

    "%temp%\getadmin.vbs"
    del "%temp%\getadmin.vbs"
    exit /B

:gotAdmin
    pushd "%CD%"
    CD /D "%~dp0"
:--------------------------------------  

echo Making sure we have administrative privileges...
net session >nul 2>&1
if %errorlevel% NEQ 0 (
    echo:
    echo Check failed
    echo You must run this script as an administrator.
    echo Please right-click on the batch file and select "Run as administrator".
    pause
    exit /b
)
echo:
echo We have admin rights. Good.

echo:
echo Setting PowerShell execution policy...
powershell -Command "Set-ExecutionPolicy Unrestricted -Scope CurrentUser -Force"

echo:
echo Run the PowerShell scripts...

for %%f in ("%script_dir%\PowerShell_scripts\*.ps1") do (
    echo Executing %%~nxf...
    powershell -File "%%~ff"
)

git config --global user.email "darkmage1991@yahoo.com"
git config --global user.name "spark_nv"

echo:
echo Importing custom registry entries...

for %%f in ("%script_dir%\Registry_Changes\*.reg") do (
    echo Importing %%~nxf...
    regedit /s "%%~ff"
)
echo:
echo Custom registry entries added successfully.

echo running AME setup...
call %script_dir%\AME_7.4\TrustedUninstaller.CLI.exe Atlas_Playbook_4

echo Setup complete.

set /a "seconds=9"
echo The computer will restart in 10 seconds...
echo:

:countdown
echo %seconds% seconds until restart...
timeout /t 1 >nul
set /a "seconds-=1"
if %seconds% gtr 0 goto countdown

echo Restarting the PC...
shutdown /r /t 3
pause
