# Check if Chocolatey is installed
if ((Get-Command choco.exe -ErrorAction SilentlyContinue) -eq $null) {
    # Install Chocolatey
    Set-ExecutionPolicy Bypass -Scope Process -Force;
    [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072;
    iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
}

# List of applications to install
$apps = @("epicgameslauncher", "iTunes", "DotNet4.6.1", "windows-sdk-10.1", "jre8", "python", "vcredist-all", "notepadplusplus", "googlechrome", "discord", "vlc", "steam", "libreoffice", "7zip", "git")

# Install applications
foreach ($app in $apps) {
    choco install $app -y
}
